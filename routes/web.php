<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\AcountInfoController;
use App\Http\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('client',ClientController::class);
Route::resource('invoice',InvoiceController::class);


Route::get('/account/edit',[AcountInfoController::class,'edit'])->name('accountinfo.edit');
Route::post('/account/edit',[AcountInfoController::class,'update'])->name('accountinfo.update');
Route::post('/invoice/paid/{id}', [InvoiceController::class, 'paid'])->name('invoice.paid');
Route::get('/invoice/downloadpdf/{id}', [InvoiceController::class, 'downloadPdf'])->name('invoice.download');

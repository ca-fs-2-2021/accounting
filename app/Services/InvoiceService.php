<?php

namespace App\Services;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class InvoiceService
{
    public const SERIAL_NUMBER_LENGHT = 6;

    private $invoiceNumber;

    public function __construct($userId)
    {
        $this->lastUserInvoice = Invoice::where('user_id', $userId)->orderBy('id', 'desc')->first();
        $this->user = User::find($userId)->first();
    }

    public function getInvoiceNumber()
    {
        if ($this->lastUserInvoice) {
            $number = $this->lastUserInvoice->number + 1;
            return $number;
        } else {
            return 1;
        }
    }

    public function getSerialNumber()
    {
        $number = $this->getInvoiceNumber();
        $length = self::SERIAL_NUMBER_LENGHT;
        $serialNumber = substr(str_repeat(0, $length) . $number, -$length);
        return $this->user->info->prefix . $serialNumber;
    }

    public function getPriceWithTax($price)
    {
        if ($this->user->info->vat_code !== null) {
            return $price * 1.21;
        }

        return $price;
    }

}

<?php

namespace App\Services;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ImageService
{
    public function uploadImage($image)
    {
        $imageFullName = $image->getClientOriginalName();
        $uploadPath = 'image/' . $imageFullName[0] . '/' . $imageFullName[1] . '/' . $imageFullName[2].'/';
        $imageUrl = $uploadPath . $imageFullName;
        $success = $image->move($uploadPath, $imageFullName);
        return $imageUrl;
    }

}

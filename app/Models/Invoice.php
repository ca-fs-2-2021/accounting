<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function items()
    {
        return $this->hasMany(InvoiceItem::class, 'invoice_id', 'id');
    }

    public function isLate()
    {

        $lastDay = date('Y-m-d', strtotime($this->created_at. ' +'.$this->payment_term.' days'));
        $today = date('Y-m-d');
        return $lastDay < $today;

    }
}

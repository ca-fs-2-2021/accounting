<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\InvoiceItem;
use Illuminate\Http\Request;
use App\Models\Invoice;
use Illuminate\Support\Facades\Auth;
use App\Services\InvoiceService;
use PDF;
class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('user_id', Auth::id())->get(); // pagination
        return view('invoice.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::where('active', 1)->where('user_id', Auth::id())->get();
        $user = Auth::user();
        return view('invoice.create', compact('clients', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'client' => 'required|integer',
            'payment_term' => 'required|integer',
            'name.*' => 'required|max:255',
            'unit.*' => 'required|integer',
            'price.*' => 'required|numeric',
            'qty.*' => 'required|integer'
        ]);

        $userId = Auth::id();
        $service = new InvoiceService($userId);
        $invoice = new Invoice();
        $invoice->number = $service->getInvoiceNumber();
        $invoice->invoice_number = $service->getSerialNumber();
        $invoice->sum_excl_tax = 0;
        $invoice->sum_incl_tax = 0;
        $invoice->user_id = $userId;
        $invoice->client_id = $request->input('client');
        $invoice->payment_term = $request->input('payment_term');
        $invoice->save();

        $invoiceId = $invoice->id;
        foreach ($request->input('name') as $key => $invoiceLine) {
            $line = new InvoiceItem();
            $line->name = $invoiceLine;
            $line->invoice_id = $invoiceId;
            $line->price_incl_tax = $service->getPriceWithTax($request->input('price')[$key]);
            $line->price_excl_tax = $request->input('price')[$key];

            $line->qty = $request->input('qty')[$key];

            $invoice->sum_excl_tax += $line->price_excl_tax * $line->qty;
            $invoice->sum_incl_tax += $line->price_incl_tax * $line->qty;

            $line->unit_id = $request->input('unit')[$key];
            $line->save();
        }

        $invoice->save();

        return redirect(route('invoice.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);
        return view('invoice.show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // TODO anuliuoti
    }

    public function paid($id)
    {
        $invoice = Invoice::where('id',$id)->where('user_id', Auth::id())->firstOrFail();
        $invoice->paid = true;
        $invoice->save();
        return redirect()->back();
    }

    public function downloadPdf($id)
    {
        $invoice = Invoice::where('id',$id)->where('user_id', Auth::id())->firstOrFail();
        $pdf = PDF::loadView('invoice.pdf.invoice', compact('invoice'));
        return $pdf->download($invoice->invoice_number);
    }
}

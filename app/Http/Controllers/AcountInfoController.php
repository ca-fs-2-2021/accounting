<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\AccountInfo;
use App\Services\ImageService;

class AcountInfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $user = Auth::user();
        return view('account.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $validate = $request->validate([
            'first_name' => 'required|max:20|min:2',
            'last_name' => 'required|max:30|min:2',
            'phone' => 'max:14',
            'code' => 'required|max:20',
            'vat_code' => 'max:20',
            'personal_id' => 'max:14',
            'address' => 'required|max:160',
            'post_code' => 'required|max:8',
            'city' => 'required|integer',
            'bank' => 'required|integer',
            'bank_account_number' => 'required|max:30',
            'prefix' => 'required|max:3',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        ]);

        $service = new ImageService();
        $imagePath = '';
        if($request->logo !== null){
            $imagePath = $service->uploadImage($request->logo);
        }



        $id = Auth::id();
        $accountInfo = AccountInfo::where('user_id', $id)->firstOrNew();

        $accountInfo->first_name = $request->input('first_name');
        $accountInfo->last_name = $request->input('last_name');
        $accountInfo->user_id = $id;
        $accountInfo->phone = $request->input('phone');
        $accountInfo->code = $request->input('code');
        $accountInfo->vat_code = $request->input('vat_code');
        $accountInfo->personal_id = $request->input('personal_id');
        $accountInfo->address = $request->input('address');
        $accountInfo->post_code = $request->input('post_code');
        $accountInfo->city_id = $request->input('city');
        $accountInfo->bank_id = $request->input('bank');
        $accountInfo->bank_account_number = $request->input('bank_account_number');
        $accountInfo->prefix = $request->input('prefix');
        $accountInfo->logo = $imagePath;
        $accountInfo->save();

        return redirect(route('accountinfo.edit'));
    }
}

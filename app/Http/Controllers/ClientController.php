<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('active', true)->where('user_id', Auth::id())->get();
//        $clients  = Client::where('active',1)->where('user_id', Auth::id())->paginate(2);

        return view('client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|max:20|min:2',
            'code' => 'required|max:20',
            'vat_code' => 'max:20',
            'address' => 'required|max:160',
            'city' => 'required|integer',
            'post_code' => 'required|max:8',
            'email' => 'email',
            'phone' => 'max:14',
        ]);

        $client = new Client();
        $client->name = $request->input('name');
        $client->code = $request->input('code');
        $client->vat_code = $request->input('vat_code');
        $client->address = $request->input('address');
        $client->city_id = $request->input('city');
        $client->user_id = Auth::id();
        $client->post_code = $request->input('post_code');
        $client->email = $request->input('email');
        $client->phone = $request->input('phone');
        $client->comment = $request->input('comment');
        $client->save();

        return redirect(route('client.index'));

    }
    // adress_enty

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::where('id', $id)->where('user_id', Auth::id())->where('active', true)->firstOrFail();
        $htmlBlock = view('client.show', compact('client'))->render();
        return response()->json(['succsess' => true, 'html' => $htmlBlock]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::where('id', $id)->where('user_id', Auth::id())->where('active', true)->firstOrFail();

        return view('client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'name' => 'required|max:20|min:2',
            'code' => 'required|max:20',
            'vat_code' => 'max:20',
            'address' => 'required|max:160',
            'city' => 'required|integer',
            'post_code' => 'required|max:8',
            'email' => 'email',
            'phone' => 'max:14',
        ]);

        $client = Client::where('id', $id)->where('user_id', Auth::id())->where('active', true)->firstOrFail();

        $client->name = $request->input('name');
        $client->code = $request->input('code');
        $client->vat_code = $request->input('vat_code');
        $client->address = $request->input('address');
        $client->city_id = $request->input('city');
        $client->post_code = $request->input('post_code');
        $client->email = $request->input('email');
        $client->phone = $request->input('phone');
        $client->comment = $request->input('comment');
        $client->save();

        return redirect(route('client.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::where('id', $id)->where('user_id', Auth::id())->firstOrFail();
        $client->active = false;
        $client->save();
        return redirect(route('client.index'));
    }
}

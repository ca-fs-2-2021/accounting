<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $keyFromAdmin = '70be27c2441543eaa0e744eb38ee7e71';
        $cityIdFromSomewhere = '4504871';
        $weather = null;
        $responce = Http::get('https://api.weatherbit.io/v2.0/current?city_id='.$cityIdFromSomewhere.'&key='.$keyFromAdmin);
        if($responce->status() === 200){
            $weather = $responce->json()['data'][0]['temp'];
        }

        return view('home', compact('weather'));
    }
}

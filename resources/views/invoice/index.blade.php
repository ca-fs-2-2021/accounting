@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-header">
                        <a  class="btn btn-info"
                            href="{{route('invoice.create')}}">
                            {{__('Create Invoice')}}
                        </a>
                    </div>
                    <ul class="list-group">
                        @foreach($invoices as $invoice)
                            <li class="list-group-item">
                                <a href="{{route('invoice.show',$invoice->id)}}">
                                    {{$invoice->invoice_number}}
                                    {{$invoice->client->name}}
                                    {{$invoice->created_at}}
                                    {{$invoice->sum_incl_tax}}
                                </a>
                                @if($invoice->isLate() && !$invoice->paid)

                                <span class="alert-danger">
                                    {{__('SKOLININKAS!!!!')}}
                                </span>
                                @endif
                                @if(!$invoice->paid)
                                    <form method="post" action="{{route('invoice.paid',$invoice->id)}}" style="float: right">
                                        @csrf
                                        @method('POST')
                                        <input type="submit" value="{{__('Paid')}}" class="btn btn-success">
                                    </form>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    {{--                    {{$clients->links()}}--}}
                </div>
            </div>
        </div>
    </div>
@endsection

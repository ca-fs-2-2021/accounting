@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{route('invoice.download', $invoice->id)}}">download PDF</a> </div>
                    <div class="card-body">

                        <form method="POST" action="{{route('invoice.store')}}">
                            @csrf
                            @method('POST')
                            <div class="row">
                                <div class="col-6">
                                    <ul class="list-unstyled">
                                        <li>{{$invoice->user->info->first_name}}</li>
                                        <li>{{$invoice->user->info->last_name}}</li>

                                        <li>{{$invoice->user->info->address ?? '' }}</li>
                                        <li>{{$invoice->user->info->city->name ?? '' }}</li>
                                        <li>{{$invoice->user->info->post_code ?? '' }}</li>
                                        <li>{{$invoice->user->info->bank_account_number ?? '' }}</li>
                                    </ul>

                                </div>
                                <div class="col-6">
                                    <div id="client-wrapper">
                                        <ul class="list-unstyled">
                                            <li>{{$invoice->client->name}}</li>
                                            <li>{{$invoice->client->code}}</li>
                                            <li>{{$invoice->client->address}}</li>
                                            <li>{{$invoice->client->city->name}} {{$invoice->client->post_code}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="invoice-items">

                                    <div class="col-3">
                                        {{__('Name')}}
                                    </div>
                                    <div class="col-3">
                                        {{__('Price')}}
                                    </div>
                                    <div class="col-3">
                                        {{__('Qty')}}
                                    </div>
                                    <div class="col-3">
                                        {{__('Sum')}}
                                    </div>

                                @foreach($invoice->items as $item)

                                        <div class="col-3">
                                            {{$item->name}}
                                        </div>
                                        <div class="col-3">
                                            {{$item->price_incl_tax}}
                                        </div>
                                        <div class="col-3">
                                            {{$item->qty}}
                                        </div>
                                        <div class="col-3">
                                            {{$item->price_incl_tax * $item->qty}}
                                        </div>

                                @endforeach

                            </div>

                            <div class="row">
                                <div class="col-6">
                                    {{__('Term to pay')}} <input type="number" name="payment_term"
                                                                 placeholder="Expl. 15d">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

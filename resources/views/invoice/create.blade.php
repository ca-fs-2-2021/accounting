@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if($errors)
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Invoice create') }}</div>
                    <div class="card-body">

                        <form method="POST" action="{{route('invoice.store')}}">
                            @csrf
                            @method('POST')
                            <div class="row">
                                <div class="col-6">
                                    <ul class="list-unstyled">
                                        <li>{{$user->info->first_name}}</li>
                                        <li>{{$user->info->last_name}}</li>

                                        <li>{{$user->info->address ?? '' }}</li>
                                        <li>{{$user->info->city->name ?? '' }}</li>
                                        <li>{{$user->info->post_code ?? '' }}</li>
                                        <li>{{$user->info->bank_account_number ?? '' }}</li>
                                    </ul>

                                </div>
                                <div class="col-6">
                                    <select name="client" id="client">
                                        <option>---------</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}" data-url="{{route('client.show',$client->id)}}">
                                                {{$client->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div id="client-wrapper">

                                    </div>
                                </div>
                            </div>
                            <div class="row" id="invoice-items">
                                <div class="col-12 invoice-line">
                                    <input required name="name[1]" placeholder="Paslauga/Preke">
                                    <input required class="line-price" name="price[1]" type="text" placeholder="Kaina">
                                    <input required class="line-qty" name="qty[1]" type="number" placeholder="Kiekis">
                                    <select name="unit[1]">
                                        <option value="1">Vnt</option>
                                        <option value="2">Val</option>
                                    </select>
                                    <input disabled name="sum[1]" value="30$">
                                </div>
                            </div>
                            <div class="btn btn-info add-invoice-line" id="add-invoice-line">+</div>

                            <div class="row">
                                <div class="col-6">
                                {{__('Term to pay')}} <input type="number" name="payment_term" placeholder="Expl. 15d">
                                </div>
                                <div class="col-6">
                                <input type="submit" class="btn btn-success" value="Create Invoice">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

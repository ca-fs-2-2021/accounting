@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form class="form" method="post" action="{{route('client.store')}}">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                        <label for="name"
                               class="col-md-4 col-form-label text-md-right">{{ __('Client Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{old('name')}}"
                                   required autocomplete="name">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="code"
                               class="col-md-4 col-form-label text-md-right">{{ __('Company Code') }}</label>

                        <div class="col-md-6">
                            <input id="code" type="text"
                                   class="form-control @error('code') is-invalid @enderror"
                                   name="code"
                                   value="{{old('code')}}"
                                   required autocomplete="code">

                            @error('code')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address"
                               class="col-md-4 col-form-label text-md-right">{{ __('Address line') }}</label>

                        <div class="col-md-6">
                            <input id="address" type="text"
                                   class="form-control @error('address') is-invalid @enderror"
                                   name="address"
                                   value="{{old('address')}}"
                                   required autocomplete="address">

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                        <div class="col-md-6">
                            <select name="city" class="form-control">
                                <option value="1">Vilnius</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="post_code"
                               class="col-md-4 col-form-label text-md-right">{{ __('Post Code') }}</label>

                        <div class="col-md-6">
                            <input id="post_code" type="text"
                                   class="form-control @error('post_code') is-invalid @enderror"
                                   name="post_code"
                                   value="{{old('post_code')}}"
                                   required autocomplete="post_code">

                            @error('post_code')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vat_code" class="col-md-4 col-form-label text-md-right">{{ __('TAX VAT ID') }}</label>

                        <div class="col-md-6">
                            <input id="vat_code" type="text" class="form-control @error('vat_code') is-invalid @enderror"
                                   name="vat_code"
                                   value="{{old('vat_code')}}"
                                   autocomplete="vat_code">

                            @error('vat_code')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{old('email')}}"
                                   required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                        <div class="col-md-6">
                            <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror"
                                   name="phone"
                                   value="{{old('phone')}}"
                                   autocomplete="phone">

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>

                        <div class="col-md-6">
                            <textarea class="form-control @error('comment') is-invalid @enderror"
                                      name="comment">{{old('comment')}}</textarea>


                            @error('comment')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="submit" class="btn btn-success" value="{{__('Create')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

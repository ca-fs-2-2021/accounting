<ul class="list-unstyled">
    <li>
        <b>
            {{$client->name}}
        </b>
    </li>
    <li>
        <i>
            {{$client->code}}
        </i>
    </li>
    <li>
        {{$client->vat_code}}
    </li>
    <li>
        {{$client->address}}
    </li>
</ul>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-header">
                        <a  class="btn btn-info"
                            href="{{route('client.create')}}">
                            {{__('Create new client')}}
                        </a>
                    </div>
                    <ul class="list-group">
                    @foreach($clients as $client)
                        <li class="list-group-item">
                            <a href="{{route('client.edit',$client->id)}}">
                                {{$client->name}}
                            </a>
                        </li>
                    @endforeach
                    </ul>
{{--                    {{$clients->links()}}--}}
                </div>
            </div>
        </div>
    </div>
@endsection

const clientSelect = document.getElementById("client");

clientSelect.onchange = function () {
    let clientSelect = document.getElementById("client");
    let clientWrapper = document.getElementById('client-wrapper');
    var selected = clientSelect.options[clientSelect.selectedIndex];
    var url = selected.getAttribute('data-url');
    fetch(url)
        .then(response => response.json())
        .then(data => {
            clientWrapper.innerHTML = data.html;
        })
        .catch((error) => {
        });
};

const addInvoiceLineButton = document.getElementById('add-invoice-line');
addInvoiceLineButton.onclick = function () {
    var invoiceItemsWrapper = document.getElementById("invoice-items").lastChild;
    var key = document.getElementsByClassName('invoice-line').length + 1;
    var newInvoiceLine = '<div class="col-12 invoice-line">' +
        '<input required="required" name="name['+key+']" placeholder="Paslauga/Preke"> ' +
        '<input required="required" name="price['+key+']" type="text" placeholder="Kaina" class="line-price"> ' +
        '<input required="required" name="qty['+key+']" type="number" placeholder="Kiekis" class="line-qty"> ' +
        '<select name="unit['+key+']">' +
        '<option value="1">Vnt</option> ' +
        '<option value="2">Val</option>' +
        '</select> ' +
        '<input disabled="disabled" name="sum['+key+']" value="30$">' +
        '</div>'


    var doc = stringToHTML(newInvoiceLine);
    document.getElementById('invoice-items').appendChild(doc);
}

var stringToHTML = function (str) {
    var parser = new DOMParser();
    var doc = parser.parseFromString(str, 'text/html');
    return doc.body;
};


$(document).ready(function(){
   $('.line-price').on('change', function(){
       var name = $(this).attr('name');
       var id = parseInt(name.match(/[0-9]+/));
       var qty = $("[name='qty["+id+"]']").val();
       var price = $("[name='price["+id+"]']").val();
       var sum  = price * qty;
       $("[name='sum["+id+"]']").val(sum)
   });
   $('.line-qty').on('change', function(){
       var name = $(this).attr('name');
       var id = parseInt(name.match(/[0-9]+/));
       var qty = $("[name='qty["+id+"]']").val();
       var price = $("[name='price["+id+"]']").val();
       var sum  = price * qty;
       $("[name='sum["+id+"]']").val(sum)
   });
});
